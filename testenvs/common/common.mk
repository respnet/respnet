# Make sure 'yq' is installed, or error out
YQ_EXISTS:=$(shell command -v yq 2> /dev/null)
ifndef YQ_EXISTS
$(error "yq is not installed, please install it")
endif

CLAB_PREFIX=$(call lc,testenv-$(PROJECT_NAME)-$(NSO_VERSION)-$(PNS))
CNT_PREFIX=$(call lc,$(CLAB_PREFIX)-$(TESTENV))

# *.clab.yml is defined as a PHONY target which means it will always be rebuilt.
# As the build of the the materialized clab topology relies on environment
# variables which we have no way of getting a timestamp for, we must rebuild in
# order to be safe.
.PHONY: quick-crpd.clab.yml
$(TESTENV).clab.yml: ../common/$(TESTENV).clab.yml
	export NSO_IMAGE=$(IMAGE_BASENAME)/nso:$(DOCKER_TAG); \
	export CLAB_PREFIX=$(CLAB_PREFIX); \
	export CNT_PREFIX=$(CNT_PREFIX); \
	export TESTENV=$(TESTENV); \
	export PROJECT_NAME=$(PROJECT_NAME); \
	export DEBUGPY=$(DEBUGPY); \
	envsubst < $< > $@

ifndef CI
SUDO=sudo
INTERACTIVE=-it
endif

# Run containerlab in a container by default, in CI as well as locally
CLAB_CONTAINER?=true
CLAB_VERSION?=latest

ifeq ($(CLAB_CONTAINER),true)
CLAB_CONTAINER_IMAGE?=$(PKG_PATH)containerlab:$(CLAB_VERSION)
CLAB_BIN:=docker run --rm $(INTERACTIVE) --privileged \
    --network host \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /var/run/netns:/var/run/netns \
    -v /etc/hosts:/etc/hosts \
    -v /var/lib/docker/containers:/var/lib/docker/containers \
	-v ${HOME}/.docker:/root/.docker \
    --pid="host" \
    -v $(PROJECT_DIR):$(PROJECT_DIR) \
    -w $(CURDIR) \
    $(CLAB_CONTAINER_IMAGE) containerlab
else
CLAB_BIN:=$(SUDO) $(CLAB_BINPATH)containerlab
endif

start: $(TESTENV).clab.yml
	$(CLAB_BIN) deploy --topo $< --log-level trace --reconfigure
	$(MAKE) help

stop: $(TESTENV).clab.yml
	$(CLAB_BIN) destroy --topo $< --log-level trace

DEVICES_CRPD?=$(shell yq -r '.topology.nodes | to_entries[] | select(.value.kind == "crpd") | .key' $(TESTENV).clab.yml)
$(addprefix console-,$(DEVICES_CRPD)): DEVICE=$(subst console-,,$@)
$(addprefix console-,$(DEVICES_CRPD)):
	@echo "Firing up serial console to $(DEVICE) (cli)"
	docker exec -it $(CNT_PREFIX)-$(DEVICE) cli

DEVICES_VRNETLAB?=$(shell yq -r '.topology.nodes | to_entries[] | select(.value.kind == "juniper_vmx") | .key' $(TESTENV).clab.yml)
$(addprefix console-,$(DEVICES_VRNETLAB)): DEVICE=$(subst console-,,$@)
$(addprefix console-,$(DEVICES_VRNETLAB)):
	@echo "Firing up serial console to $(DEVICE) (telnet to port 5000)"
	telnet $$(docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(CNT_PREFIX)-$(DEVICE)) 5000

DEVICES_PC?=$(shell yq -r '.topology.nodes | to_entries[] | select(.value.kind == "linux") | .key' $(TESTENV).clab.yml)
$(addprefix console-,$(DEVICES_PC)): DEVICE=$(subst console-,,$@)
$(addprefix console-,$(DEVICES_PC)):
	@echo "Firing up interactive shell on $(DEVICE) (/bin/bash -l)"
	docker exec -it $(CNT_PREFIX)-$(DEVICE) bash -l

compare-config: compare-check-missing
	status=0; \
	for F in $$(ls output/* | sed 's/output\///'); do \
		echo "Comparing expected/$$F with output/$$F"; \
		diff -u expected/$$F output/$$F; \
		[ $$? -ne 0 ] && status=1; \
	done; \
	exit $${status}

compare-check-missing:
	status=0; \
	for F in $$(ls expected/* | sed 's/expected\///'); do \
		echo "Checking existence of output/$$F"; \
		if [ -f output/$$F ] ; then \
		   echo "File output/$$F found"; \
		else echo "File output/$$F is missing"; status=1;\
		fi; \
	done; \
	exit $${status}

save-output:
	for NSO_CNT in $$(docker ps --format '{{.Names}}' --filter label=com.cisco.nso.testenv.name=$(CNT_PREFIX) --filter label=com.cisco.nso.testenv.type=nso); do \
		NSO=$$(echo $$NSO_CNT | sed 's/.*-nso//'); \
		$(MAKE) save-output-nso NSO=$$NSO; \
	done

# TODO: add save-output for CFS
save-output-nso:
	NSO_CNT=$(CNT_PREFIX)-nso$(NSO) ../save-output.sh --get-config /devices/device --output output/1$(NSO)-device-config.xml
	NSO_CNT=$(CNT_PREFIX)-nso$(NSO) ../save-output.sh --get-config /nodes --output output/1$(NSO)-node-config.xml
	NSO_CNT=$(CNT_PREFIX)-nso$(NSO) ../save-output.sh --get-config /rfs --output output/1$(NSO)-rfs-config.xml

update-expected:
	rm -f expected/*
	cp output/* expected/

configure-nso:
	$(MAKE) wait-started-nso
	$(MAKE) configure-common-config
	$(MAKE) copy-ssh-key
	$(MAKE) configure-ci-user

copy-ssh-key:
	docker exec $(CNT_PREFIX)-nso$(NSO) mkdir -p /var/ncs/homes/ci/.ssh
	docker cp ../common/ssh-key-ci.pub $(CNT_PREFIX)-nso$(NSO):/var/ncs/homes/ci/.ssh/authorized_keys

configure-common-config:
	$(MAKE) loadconf FILE=../common/common-config.xml

configure-ci-user:
	$(MAKE) loadconf FILE=../common/ci-user.xml

remove-service:
	@echo "TODO"

# Rewrite of the docker port output...
DPR=| sed -e "s/0.0.0.0/$${V4:-127.0.0.1}/g" -e "s/\[::\]/[$${V6:-::1}]/" -e 's!\(.*\)! - http://\1/graphite!'
GLOBAL_IPV6_ADDRESS=$(shell ip -6 addr | awk '{print $$2}' | grep -P '^(?!fe80)[[:alnum:]]{4}:.*/64' | cut -d '/' -f1)
GLOBAL_IPV4_ADDRESS=$(shell ip -4 addr | awk '{print $$2}' | grep -P '^(?!127)[[:digit:]]{1,3}\.' | cut -d '/' -f1)
help:
	@echo "You can access the network devices serial console with the 'console-<DEVICE>' target:"
	@echo " $(DEVICES_VRNETLAB) $(DEVICES_CRPD) $(DEVICES_PC)"
	@echo "For example to access CR-1 Console use 'console-CR-1':"
	@echo " make -C testenvs/$(TESTENV) console-CR-1"
	@echo ""
	@echo "You can access the Graphite Topology Web UI on these local ports:"
	@docker port $(CNT_PREFIX)-graphite 80/tcp $(DPR)
ifneq ($(GLOBAL_IPV6_ADDRESS)$(GLOBAL_IPV4_ADDRESS),)
	@echo "We found these additional addresses you can try:"
	@for v4 in $(GLOBAL_IPV4_ADDRESS); do \
		export V4=$${v4}; \
		docker port $(CNT_PREFIX)-graphite 80/tcp | grep "0\.0\.0\.0" $(DPR); \
	done
	@for v6 in $(GLOBAL_IPV6_ADDRESS); do \
		export V6=$${v6}; \
		docker port $(CNT_PREFIX)-graphite 80/tcp | grep "\[::\]" $(DPR); \
	done
	@echo ""
	@echo "You can access the NSO CLI with the 'cli' target:"
	@echo " make -C testenvs/$(TESTENV) cli"
endif
