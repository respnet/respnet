# SOmewhat REalistic Service PrOvider Network

This is an example of a somewhat realistic service provider network and how to
automate it using Cisco NSO. It showcases:

- realistic configuration for an SP MPLS network
  - it actually works and builds a network that implements L3VPN MPLS VPNs
- it exposes a northbound API using the industry standard IETF L3VPN YANG model
  for ordering L3VPNs as well as customized models for configuring
  infrastructure services
- multi-vendor, supporting Cisco IOS XR devices, Juniper and Nokia routers
- test environments including
  - containerized routers for checking control plane etc is working
    - but rather limited config coverage support, e.g. ACLs are not supported on
      JUNOS cRPD
  - virtual routers for good configuration coverage support, e.g. ACLs on cRPD
    - but virtualized routers are CPU & RAM heavy
  - netsims for complete config coverage support
    - but no control plane etc so no way of validating operational state
  - pick the test environment you need for the work you are currently doing
  - the topology itself, like which routers are connected to what is separately
    defined so it is simple to maintain a topology that can be started with
    these variations of devices


# YANG model design


# TODO
- [ ] nodes / RFS services layers in place
- [ ] network infrastructure services, v1
  - [ ] base-config
  - [ ] backbone-interface
  - [ ] ibgp-neighbor for RR-clients & full-mesh
  - [ ] carry VPNv4 & VPNv6 in iBGP (same topo for now!?)
  - [ ] do MPLS SR
- [x] device-automaton
- [ ] use initial-credentials on device-automaton
- [ ] L3VPN service
- [ ] CPEs
